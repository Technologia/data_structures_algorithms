
# Insertion Sort

def insertion_sort(alist):
    for i in range(1, len(alist)):
        key = alist[i]
        j = i - 1
        while j >= 0 and alist[j] > key:
            alist[j + 1] = alist[j]
            j = j - 1
        alist[j + 1] = key
    return alist


# Merge Sort

def merge(alist, start, mid, end):
    temp1 = []
    temp2 = []
    for i in range(start, mid):
        temp1.append(alist[i])
    for j in range(mid, end):
        temp2.append(alist[j])
    for k in range(start, end):
        if len(temp1) == 0:
            alist[k] = temp2.pop(0)
        elif len(temp2) == 0:
            alist[k] = temp1.pop(0)
        elif temp1[0] > temp2[0]:
            alist[k] = temp2.pop(0)
        else:
            alist[k] = temp1.pop(0)


def merge_sort(alist, start, end):
    if start < end - 1:
        mid = (start + end) / 2
        merge_sort(alist, start, mid)
        merge_sort(alist, mid, end)
        merge(alist, start, mid, end)
    return alist


# Maximum Subarray


def max_crossing_subarray(alist, start, mid, end):
    left_sum = alist[mid - 1]
    current_left_sum = alist[mid - 1]
    left_index = mid - 1
    for i in range(mid - 2, start - 1, -1):
        current_left_sum = current_left_sum + alist[i]
        if current_left_sum > left_sum:
            left_sum = current_left_sum
            left_index = i
    right_sum = alist[mid]
    current_right_sum = alist[mid]
    right_index = mid
    for j in range(mid + 1, end):
        current_right_sum = current_right_sum + alist[j]
        if current_right_sum > right_sum:
            right_sum = current_right_sum
            right_index = j
    return (left_index, right_index + 1, left_sum + right_sum)


def max_subarray(alist, start, mid, end):
    if start == end - 1:
        return (start, end, alist[start])
    else:
        left = max_subarray(alist, start, (start + mid) / 2,  mid)
        right = max_subarray(alist, mid, (mid + end) / 2, end)
        crossing = max_crossing_subarray(alist, start, mid, end)
        if max(left[2], right[2], crossing[2]) == left[2]:
            return left
        elif max(left[2], right[2], crossing[2]) == right[2]:
            return right
        else:
            return crossing

# Heaps

class Heap:

    def __init__(self, array):
        self.array = array
        self.size = len(array)

    def __repr__(self):
        for i in range(len(self.array)):
            print "%d (%d)" % (self.array[i], i)
        return "END"


def heap_parent(i):
    return (i - 1) / 2

def heap_left(i):
    return i * 2 + 1

def heap_right(i):
    return i * 2 + 2

def heapify(hp, i):
    left = heap_left(i)
    right = heap_right(i)
    value = hp.array[i]
    largest = i
    if left < hp.size and hp.array[left] > value:
        largest = left
    if right < hp.size and hp.array[right] > hp.array[largest]:
        largest = right
    if largest != i:
        hp.array[i] = hp.array[largest]
        hp.array[largest] = value
        heapify(hp, largest)

def build_max_heap(lst):
    hp = Heap(lst)
    for i in range(hp.size / 2, -1, -1):
        heapify(hp, i)
    return hp


def heapsort(lst):
    hp = build_max_heap(lst)
    for i in range(hp.size - 1, -1, -1):
        temp = hp.array[i]
        hp.array[i] = hp.array[0]
        hp.array[0] = temp
        hp.size = hp.size - 1
        heapify(hp, 0)

# Priority Queues

def heap_maximum(hp):
    return hp.array[0]

def heap_extract_max(hp):
    if hp.size < 1:
        print 'empty heap'
        return
    value = hp.array[0]
    hp.array[0] = hp.array[hp.size - 1]
    hp.size = hp.size - 1
    heapify(hp, 0)
    return value

def heap_increase_key(hp, i, key):
    if key < hp.array[i]:
        print "invalid"
    while i > 0 and hp.array[i] > hp.array[heap_parent(i)]:
        temp = hp.array[heap_parent(i)]
        hp.array[heap_parent(i)] = hp.array[i]
        hp.array[i] = temp
        i = heap_parent(i)

def max_heap_insert(hp, key):
    hp.size = hp.size + 1
    hp.array.append(key)
    heap_increase_key(hp, hp.size - 1, key)

# Quicksort

def partition(lst, start, end):
    pivot = lst[end - 1]
    i = start
    for j in range(start, end - 1):
        if lst[j] < pivot:
            temp = lst[j]
            lst[j] = lst[i]
            lst[i] = temp
            i = i + 1
    lst[end - 1] = lst[i]
    lst[i] = pivot
    return i

def quicksort(lst, start, end):
    if start < end - 1:
        pivot = partition(lst, start, end)
        quicksort(lst, start, pivot)
        quicksort(lst, pivot + 1, end)


# Counting Sort

def counting_sort(lst, k):
    new_list = []
    sorted_lst = [None] * len(lst)
    for i in range(k + 1):
        new_list.append(0)
    for j in lst:
        new_list[j] = new_list[j] + 1
    for k in range(1, len(new_list)):
        new_list[k] = new_list[k - 1] + new_list[k]
    for l in reversed(lst):
        sorted_lst[new_list[l] - 1] = l
        new_list[l] = new_list[l] - 1
    return sorted_lst


# Radix Sort

def radix_sort(lst, k):
    pass


# Select

def select(lst, start, end, index):
    if start == end - 1:
        return lst[start]
    q = partition(lst, start, end)
    l = q - start
    if l == index:
        return lst[q]
    elif l < index:
        return select(lst, q + 1, end, index - l - 1)
    else:
        return select(lst, start, q, index)

# Data Structures

class Stack:

    def __init__(self):
        self.data = []

    def is_empty(self):
        if self.data == []:
            return True
        return False

    def push(self, item):
        self.data.append(item)

    def pop(self, item):
        return self.data.pop()

class Queue:

    def __init__(self):
        self.data = [None] * 100
        self.head = 0
        self.tail = 0

    def enqueue(self, item):
        self.data[self.tail] = item  


if __name__ == "__main__":
    pass