import unittest
from dsa import *
import random

# Random array generator
def list_generator():
    lst = []
    for i in range(10):
        lst.append(random.randint(-1000,1000))
    return lst

# Checks if list is sorted
def sort_list_checker(alist):
    for i in range(1, len(alist)):
        if alist[i - 1] > alist[i]:
            return False
    return True

# Learning how to use unittest

class GeneralTestCase(unittest.TestCase):
	def setUp(self):
		self.lst = []
		for i in range(10):
			self.lst.append(list_generator())

	def tearDown(self):
		pass

	def test_sorted(self):
		for i in self.lst:
			self.assertTrue(sort_list_checker(merge_sort(i, 0,
				len(i))))

if __name__ == '__main__':
	# Bare minimum
	# unittest.main()
	suite = unittest.TestLoader().loadTestsFromTestCase(GeneralTestCase)
	unittest.TextTestRunner(verbosity=2).run(suite)	

	# for i in range(1000):
	# 	lst = list_generator()
	# 	quicksort(lst, 0, len(lst))
	# 	print sort_list_checker(lst)
	lst2 = list_generator()
	print lst2
	print select(lst2, 0, len(lst2), 5)
	print lst2
	# print max_subarray(lst, 0, len(lst) / 2, len(lst))
	# hp = build_max_heap(lst)
	# print hp
	# print heap_extract_max(hp)
	# print hp

